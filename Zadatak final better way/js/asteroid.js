/*
* Main class
*/
function Asteroid() {
	this.apiEndPoint = 'https://api.nasa.gov/neo/rest/v1/feed?';
	this.apiKey = 'x0HeIJzRCLm3lj0zrfXt2LltusKVCO7aoHmRkVq2';
	this.startDate = null;
	this.endDate = null;
	this.tableRows = 10;
	this.sortBy = 'name';
	this.sortDirection = true;
	this.data = {};
	this.formatData = [];
}

/*
* init method
*/
Asteroid.prototype.init = function() {


};

/*
* method for check dates
*/
Asteroid.prototype.checkInputDates = function() {
	// date validation is being done via html5 input field
	// todo use reg exp
	this.startDate = document.getElementById("start_date").value;
	this.endDate = document.getElementById("end_date").value;

	//count difference between Start Date and End Date
	const start_date_obj = new Date(this.startDate);
	const end_date_obj = new Date(this.endDate);
	let difference = Math.floor((Date.UTC(end_date_obj.getFullYear(), end_date_obj.getMonth(), end_date_obj.getDate()) - Date.UTC(start_date_obj.getFullYear(), start_date_obj.getMonth(), start_date_obj.getDate()) ) /(1000 * 60 * 60 * 24));

	if(difference <= 7){
		asteroid.fetchData();
	}
	else{
		alert("End date ne sme biti vise od 7 dana!");
	}

};

/*
* method for fetching data from remote api
*/
Asteroid.prototype.fetchData = function() {
	//fetch api
	const uri = this.apiEndPoint + 'start_date=' + this.startDate + '&end_date=' + this.endDate + '&api_key=' + this.apiKey;
	const errorBlock = document.getElementById('еrror-block');

	fetch(uri)
	.then(response => {
		return response.json();
	})
	.then(data => {

		this.data = data;
		asteroid.formatedData();
	})
	.catch(error => {
		errorBlock.innerHTML = error;
	});
};
/*
* method for creating array to be able to sort it with Sort Data method
*/
Asteroid.prototype.formatedData = function(){
	console.log("fdsfsd");
	let dates = Object.keys(this.data['near_earth_objects']);

	let dateObjectsArray = dates.map((dateKey, index)=>{
		return this.data["near_earth_objects"][dateKey][index];
	});

	console.log(dateObjectsArray)
	this.formatData = dateObjectsArray;
	asteroid.drawTable();

}
/*
* method for drawing table
*/
Asteroid.prototype.drawTable = function() {
	//console.log(this.data[0].name);
	asteroid.selectAutopopulate(this.formatData);
	returned_asteroids.style.display="block";
		let html = "<table id='table'><th onclick='asteroid.sortData'>Date</th><th onclick='sortTable(1)'>Name</th><th onclick='sortTable(2)'>Kilometar per hour</th><th onclick='sortTable(3)'>Diametar min</th><th onclick='sortTable(4)'>Diametar max</th>";
		console.log(this.formatData)
		for (var i = 0; i < this.formatData.length; i++) {
			let getData = this.formatData[i];
				html+="<tr>";
				html+="<td>"+getData.name+"</td>";
				html+="<td>"+getData.name+"</td>";
				html+="<td>"+getData.close_approach_data[0].relative_velocity.kilometers_per_hour+"</td>";
				html+="<td>"+getData.estimated_diameter.meters.estimated_diameter_min+"</td>";
				html+="<td>"+getData.estimated_diameter.meters.estimated_diameter_max+"</td>";
				html+="</tr>";
		}

		html+="</table>";
		document.getElementById("table").innerHTML=html;


};
/*
* response object should be formated like following to be able to do proper sort with sortData
*/
/*
this.items = [
  { name: 'Edward', value: 21 },
  { name: 'Sharpe', value: 37 },
  { name: 'And', value: 45 },
  { name: 'The', value: -12 },
  { name: 'Magnetic', value: 13 },
  { name: 'Zeros', value: 37 }
];
*/
/*
* method for sorting data in the table
*/
Asteroid.prototype.sortData = function(fieldName) {
	this.items.sort(function (a, b) {
		return a[fieldName] - b[fieldName];
	});
};
Asteroid.prototype.selectAutopopulate =function(test){

	let select = $('#autopopulateselect').selectize({
						maxItems: null,
						valueField: 'id',
						labelField: 'name',
						searchField: 'name',
						options: test,
						create: false,
						onChange: function(){

							Asteroid.prototype.createTableFromSelect();

						}

					});

			}
/*
* method for creating table with choosen asteroids from dropdown list
*/
Asteroid.prototype.createTableFromSelect = function(){

}
let asteroid = new Asteroid();
asteroid.init();
