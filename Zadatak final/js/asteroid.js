/*
*Function for limit 7 days difference between Start date and End date
*/
function dates(){
	let start_date = document.getElementById("start_date").value;
	let end_date = document.getElementById("end_date").value;

	const start_date_obj = new Date(start_date);
	const end_date_obj = new Date(end_date);
	let difference = Math.floor((Date.UTC(end_date_obj.getFullYear(), end_date_obj.getMonth(), end_date_obj.getDate()) - Date.UTC(start_date_obj.getFullYear(), start_date_obj.getMonth(), start_date_obj.getDate()) ) /(1000 * 60 * 60 * 24));

	if(difference <= 7){
		getall(start_date, end_date);
	}
	else{
		alert("End date ne sme biti vise od 7 dana!");
	}
}
/*
*Function to get all items from api response
*/
function getall(new_date_start, new_date_end){

	const api_call = fetch(`https://api.nasa.gov/neo/rest/v1/feed?start_date=${new_date_start}&end_date=${new_date_end}&api_key=x0HeIJzRCLm3lj0zrfXt2LltusKVCO7aoHmRkVq2`);

	const errorBlock = document.getElementById('еrror-block');

	api_call
	  .then(response => {
	    return response.json();
	  })
	  .then(data => {
	    objects(data);
	  })
	  .catch(error => {
	    errorBlock.innerHTML = error;
	  });
}
/*
* Loop through all items from api response and get all items with "is_potentially_hazardous_asteroid" = true
*/
function objects(data) {
	const hazardous_true_array = [];
	let filter_data = [];
	for(let date in data.near_earth_objects) {
		let dataArray = data.near_earth_objects[date];
		for(let link in dataArray) {
			let dataItem = dataArray[link];
			dataItem["date"] = date;

			if(dataItem.is_potentially_hazardous_asteroid === true) {

				for(let diameter in dataItem["estimated_diameter"] ){
					let diametar_item = dataItem["estimated_diameter"][diameter];
					dataItem["diametar_item_max"] = diametar_item["estimated_diameter_max"];
					dataItem["diametar_item_min"] = diametar_item["estimated_diameter_min"];

					if(!filter_data.includes(dataItem["name"]))
					{
						filter_data.push(dataItem["name"]);
						hazardous_true_array.push(dataItem);
				}
				}
				returned_asteroids.style.display="block";
			} else {
			}
		}
	}

		localStorage.setItem("hazardous_stored", JSON.stringify(hazardous_true_array));
		createTable();
		autopopulateselect();
}
/*
*Create table with all asteroids
*/
function createTable(){
		let data = JSON.parse(localStorage.getItem("hazardous_stored"));
		$(document).ready(function () {
	    var html = "<table id='tabela'><th onclick='sortTable(0)'>Date</th><th onclick='sortTable(1)'>Name</th><th onclick='sortTable(2)'>Kilometar per hour</th><th onclick='sortTable(3)'>Diametar min</th><th onclick='sortTable(4)'>Diametar max</th>";
	    for (var i = 0; i < data.length; i++) {
	        html+="<tr>";
	        html+="<td>"+data[i].date+"</td>";
	        html+="<td>"+data[i].name+"</td>";
					html+="<td>"+data[i].close_approach_data[0].relative_velocity.kilometers_per_hour+"</td>";
	        html+="<td>"+data[i].diametar_item_min+"</td>";
					html+="<td>"+data[i].diametar_item_max+"</td>";
	        html+="</tr>";
	    }
	    html+="</table>";
	    $("#table").html(pagination(html));
	});
}
/*
* Function for sorting table per column
*/
function sortTable(n) {
  let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("tabela");
  switching = true;
  dir = "asc";
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
/*
* Function for pagination
*/
function pagination(html){
	let rows = JSON.parse(localStorage.getItem("hazardous_stored"));
	if(rows.length > 10){
		return html;
	}
	else{
		return html;
	}
}
/*
* Populate select box with Asteroid names for specific date range
*/
function autopopulateselect(){
	let data = JSON.parse(localStorage.getItem("hazardous_stored"));

	$(document).ready(function () {
		let dataLength = data.length;
		var datalist = "<input type='text' name='asteroid_select' list='asteroids' id='datalist' onchange='choosenasteroid(value)'></div><datalist id='asteroids'>";
		for (var i = 0; i < dataLength; i++) {
		    datalist+="<option>"+data[i].name+"</option>";
		}
		datalist+="</datalist>";
		$("#datalist").html(datalist);
		$('input[name=asteroid_select]').focusin(function() {
			$('input[name=asteroid_select]').val('');
		});
	});
}
/*
* Create table with selected Asteroid and delete button
*/
function choosenasteroid(value){

	let selectedAsteroid = "";

	 selectedAsteroid += "<table id='selectedAsteroid_table'><th>Name</th><th>Delete Asteroid</th><tr><td>"+value+"</td><td><input type='button' value='Delete Asteroid'></td></tr>";
	$("#selectedAsteroid").html(selectedAsteroid);

}
function get_self_api(){
	let close_approach_data = JSON.parse(localStorage.getItem("hazardous_stored"));

	for(let i=0; i<close_approach_data.length;i++){
		const chart_api_call = fetch(close_approach_data[i].links.self);

		const errorBlock = document.getElementById('еrror-block');
		chart_api_call
		  .then(response => {
		    return response.json();
		  })
		  .then(chart_info => {
		    chart_data(chart_info);
		  })
		  .catch(error => {
		    errorBlock.innerHTML = error;
		  });
	}
}
function chart_data(chart_info){
	const close_approach_data = [];

	for(let item in chart_info.close_approach_data) {
		let dataArray = chart_info.close_approach_data[item];
	}
}
